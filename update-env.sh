#!/bin/sh

set -ex

env_txts=`ls uboot.*.txt`
for e in $env_txts; do
	envfile=`echo $e | sed -e 's/\.txt$//'`
	mkenvimage -o $envfile -s `stat --printf="%s" $envfile` $e
done
